# Treasurer Tool

Node.js initial demo

## Install

1. Install Node.js on system (```npm install``` may be needed)
2. Clone/download repository
3. ``` cd \path\to\repo ```
4. ``` node server.js ```
5. Go to http://localhost:9250 with browser	

## Implements 

* [Cozy.io tutorial](https://docs.cozy.io/en/hack/getting-started/first-app.html)